/*Creates the database*/
CREATE DATABASE bjmac_deliverables;

/*use the new database*/
USE bjmac_deliverables;

--
-- Database tables from Codes Database (from CISAdmin)
--

/*Table structure for table CodeType*/
CREATE TABLE IF NOT EXISTS `CodeType` (
  `codeTypeId` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

/*Dumping data for table CodeType*/
INSERT INTO `CodeType` (`codeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'Member Levels', '', '', '', '', '');

/*AUTO_INCREMENT for table CodeType*/
ALTER TABLE `CodeType` MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT = 2;

/*Table structure for table CodeValue*/
CREATE TABLE IF NOT EXISTS `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL PRIMARY KEY,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL,
  FOREIGN KEY (codeTypeId) REFERENCES CodeType(codeTypeId)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

/*Dumping data for table CodeValue*/
INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, '', 'User', '', '', '2016-02-03 17:00:00', '', '', '');

/*AUTO_INCREMENT for table CodeValue*/
ALTER TABLE `CodeValue` MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 3;

/*Table structure for table UserAccess*/
CREATE TABLE IF NOT EXISTS `UserAccess` (
  `UserAccessId` int(3) NOT NULL PRIMARY KEY,
  `username` varchar(100) NOT NULL UNIQUE KEY COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '2' COMMENT 'CodeValue #2 for Customer.',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.',
  FOREIGN KEY (userTypeCode) REFERENCES CodeValue(codeValueSequence)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Dumping data for table UserAccess*/
INSERT INTO `UserAccess` (`UserAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) 
VALUES (1, 'user', md5('123'), 1, '2016-02-03 17:00:00');

/*AUTO_INCREMENT for table UserAccess*/
ALTER TABLE `UserAccess` MODIFY `UserAccessId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

--
-- Database table created for grade application
--

/*table for deliverables*/
CREATE TABLE IF NOT EXISTS `deliverables` (
  `id` INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(25) NOT NULL,
  `weight` DOUBLE NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*create a user in database*/
grant select, insert, update, delete on bjmac_deliverables.*
             to 'deliverable'@'localhost'
             identified by 'password';
flush privileges;
