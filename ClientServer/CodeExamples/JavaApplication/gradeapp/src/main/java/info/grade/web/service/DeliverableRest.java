package info.grade.web.service;

import info.grade.model.Deliverables;
import info.grade.service.GradesService;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Component
@Path("/bjmac_deliverable")
@Scope("request")
public class DeliverableRest {
    
    @Resource
    private final GradesService service; //JPA Repository Access
    
    public DeliverableRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.service = applicationContext.getBean(GradesService.class);
    }
    
    @GET
    @Path("/deliverables")
    @Produces("application/json")
    public Response getDeliverables() {
        //response code
        int responseCode;
        
        //get deliverables
        ArrayList<Deliverables> deliverables = service.getDeliverables();
        
        //convert list to json
        JSONObject json = new JSONObject();
        JSONObject object;
        JSONArray array = new JSONArray();
        try{
            if(deliverables.isEmpty()) {
                responseCode = 204;
            } else {
                responseCode = 200;
                for(Deliverables deliverable : deliverables) {
                    object = new JSONObject();
                    object.put("id", deliverable.getId());
                    object.put("name", deliverable.getName());
                    object.put("weight", deliverable.getWeight());
                    array.put(object);
                }
                json.put("deliverables", array);
            }
        } catch(JSONException jse) {
            responseCode = 501;
        }
        
        return Response.status(responseCode).entity(json.toString()).build();
    }
}
