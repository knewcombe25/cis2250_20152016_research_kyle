package info.grade.dao;

import info.grade.dao.util.ConnectionUtils;
import info.grade.dao.util.DbUtils;
import info.grade.model.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DeliverablesDAO {

    public static synchronized double getTotalWeight() {
        double totalWeight = 0;

        PreparedStatement ps = null;
        String sql;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(new DatabaseConnection("bjmac_deliverables", "deliverable_admin", "password")); //set connection
            sql = "SELECT sum(`weight`) as totalWeight FROM `deliverables`";
            ps = conn.prepareStatement(sql); //run sql statement
            ResultSet rs = ps.executeQuery(); //get statment results
            while (rs.next()) {
                totalWeight = rs.getInt("totalWeight");
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING TOTAL WEIGHT: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        return totalWeight;
    }
}
