package app.gradeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * @author Kody. Duncan
 * @since 1/29/2016
 * <p/>
 * Allows the use of a start screen when loading application.
 */

public class StartScreen extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startscreen);

        Thread startThread = new Thread() {

            @Override
            public void run() {
                try {
                    super.run();
                    sleep(5000); //delays for seconds
                } catch (Exception e) {
                } finally {
                    Intent loginIntent = new Intent(StartScreen.this, UserLogin.class);
                    startActivity(loginIntent);
                    finish();
                }
            }
        };
        startThread.start();
    }
}
