package app.gradeapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

public class ActivityView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getStringExtra("layout") != null) {
            String layout = getIntent().getStringExtra("layout");
            if (layout.equalsIgnoreCase("deliverables")) {
                setContentView(R.layout.activity_deliverables);

                if (!isConnected(this)) {
                    showAlertDialog(this, "Network Error!", "No network found." +
                            "\nPlease Connect To A Network.");
                    return;
                }

                //get deliverable connection
                Connection connection = new Connection("deliverables");
                connection.execute("");
                try {
                    //get json data from connection
                    Gson gson = new Gson();
                    Deliverable deliverable = gson.fromJson(connection.get(), Deliverable.class); //automatically converts json string to ArrayList of deliverable objects
                    Log.d("TOTAL OBJECTS", String.valueOf(deliverable.getDeliverables().size()));

                    //get movies table
                    TableLayout moviesTable = (TableLayout) findViewById(R.id.deliverables_table);

                    //populate movies table
                    if (!deliverable.getDeliverables().isEmpty()) {
                        for (Deliverable deliverable1 : deliverable.getDeliverables()) {
                            TableRow tableRow = new TableRow(this);
                            TextView nameView = new TextView(this);
                            TextView weightView = new TextView(this);
                            nameView.setText(deliverable1.getName());
                            weightView.setText(String.valueOf(deliverable1.getWeight()));
                            tableRow.addView(nameView);
                            tableRow.addView(weightView);
                            moviesTable.addView(tableRow);
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //find selected id
        int id = item.getItemId();
        switch (id) {
            case R.id.back:
                finish();
                return true;
            case R.id.close:
                System.exit(0);
                return true;
            default:
                Log.d("SETTINGS MENU ERROR", "Option Not Found!");
                break;
        }

        //return value
        return super.onOptionsItemSelected(item);
    }

    /*
     * Provides a alert dialog to any activity.
    */
    public static void showAlertDialog(Context context, String title, String message) {

        //create an alert dialog for message displaying
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    /*
     * Checks if internet is connected.
     *
     * http://developer.android.com/training/monitoring-device-state/connectivity-monitoring.html
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            Connection newConnection = new Connection("http://bjmac.hccis.info:8080/cisadmin/rest/useraccess/bjmac_codes");
            newConnection.execute();
            try {
                String results = newConnection.get();
                if (!results.isEmpty() && results.equalsIgnoreCase("0")) {
                    isConnected = true;
                } else {
                    isConnected = false;
                }
            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
        return isConnected;
    }
}