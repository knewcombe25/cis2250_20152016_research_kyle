﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneAppEx
{
    public partial class MainPage : PhoneApplicationPage
    {
        private String gradeOne;
        private String gradeTwo;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }
        //Calculate button
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            gradeOne = textBox1.Text;
            gradeTwo = textBox2.Text;

            NavigationService.Navigate(new Uri("/Results.xaml?classOneGrade=" + gradeOne + "&classTwoGrade=" + gradeTwo, UriKind.RelativeOrAbsolute));

        }
    }
}