﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace PhoneAppEx
{
    public partial class Results : PhoneApplicationPage
    {
        private float gradeOne;
        private float gradeTwo;

        private const float gradeOneWeight = .80f;
        private const float gradeTwoWeight = .20f;


        public Results()
        {
            InitializeComponent();
        }

        //
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            String gradeOneString = "";
            String gradeTwoString = "";

            NavigationContext.QueryString.TryGetValue("classOneGrade", out gradeOneString);
            NavigationContext.QueryString.TryGetValue("classTwoGrade", out gradeTwoString);
            gradeOne = float.Parse(gradeOneString);
            gradeTwo = float.Parse(gradeTwoString);
            textBlock1.Text = "Average = " + calculateTotalGrade().ToString();

        }
        //Calculates average
        public float calculateTotalGrade()
        {
            float finalMark = (gradeOneWeight * gradeOne) + (gradeTwoWeight * gradeTwo);
            return finalMark;
        }

    }
}