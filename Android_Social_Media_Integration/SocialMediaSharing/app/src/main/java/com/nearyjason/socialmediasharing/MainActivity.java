package com.nearyjason.socialmediasharing;
/*@author Jason Neary
  @Since February 3, 2016
  This short program shows intent sharing with social media. It contains examples
  of sharing via the chooser method with both images and text. This properly transfers the
  text and images to social media apps Facebook Messenger and twitter. It just launches the regular
  Facebook app without any content.
* */
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button shareButton = null;
    private EditText input = null;
    private Button shareImageButton = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        shareButton = (Button)findViewById(R.id.shareTextButton);
        input = (EditText)findViewById(R.id.shareText);
        shareImageButton = (Button) findViewById(R.id.shareImageButton);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // creates a new intent when the shareButton is clicked.
                Intent textIntent = new Intent();
                //Specifies that the purpose of this intent is to send data
                textIntent.setAction(Intent.ACTION_SEND);
                //gets the text from the EditText box
                textIntent.putExtra(Intent.EXTRA_TEXT, input.getText().toString());
                //Specifies type of data to be sent
                textIntent.setType("text/plain");
                //Opens the chooser with and sends the data within the intent to the selected application.
                startActivity(Intent.createChooser(textIntent, "Share text using..."));
            }
        });
        shareImageButton.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View view) {
                //creates a new Uri object from a String
                Uri imageUri = Uri.parse("android.resource://com.nearyjason.socialmediasharing/drawable/" + R.drawable.andyandroid);
                //creates a new intent
                Intent imageIntent = new Intent(Intent.ACTION_SEND);
                //specifies the type of data being sent
                imageIntent.setType("image/*");
                //adds the image
                imageIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                //Opens chooser
                startActivity(Intent.createChooser(imageIntent, "Share image using..."));
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
